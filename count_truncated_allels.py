#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, MF 1, fuchss@rki.de)

VERSION = "0.0.9"
import os
import argparse
import re
from Bio import SeqIO
import sys
from collections import defaultdict

def parse_args():
    parser = argparse.ArgumentParser(prog="", description="", )
    parser.add_argument('fasta', metavar='FASTA_FILE', help="fasta files containing  allele sequences (one per gene)", type=str, nargs="+")
    parser.add_argument('--start', metavar="STR", help="start codon(s) (default: 'TTG', 'GTG', 'ATA', 'ATG', 'ATC', 'ATT', 'CTG') ", type=str, nargs="+", default=['TTG', 'GTG', 'ATA', 'ATG', 'ATC', 'ATT', 'CTG'])
    parser.add_argument('--stop', metavar="STR", help="stop codon(s) (default: 'TAA', 'TAG', 'TGA')", type=str, nargs="+", default=['TAA', 'TAG', 'TGA'])
    parser.add_argument('--matrix', metavar="FILE", help="calculate allele frequencies based on tab-delimited allele matrix e.g. provided by Seqsphere+ (default: none)", type=str, default=None)
    parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)
    return parser.parse_args()

def iter_fasta(fname):
    for seq_record in SeqIO.parse(fname, "fasta"):
        yield seq_record.id, str(seq_record.seq)

def rev_compl(seq):
    return seq.lower().replace("a", "T").replace("t", "A").replace("g", "C").replace("c", "G")[::-1]

def has_start_and_stop(seq, starts, stops, rev=False):
    if rev:
        seq = rev_compl(seq)
    if seq.startswith(starts) and seq.endswith(stops):
        return True
    else:
        return False

def has_internal_stops(seq, stops, rev=False):
    if rev:
        seq = rev_compl(seq)
    codons = set([seq[i:i+3] for i in range(0, len(seq)-3, 3)])
    return len(codons.intersection(stops)) > 0

def first_internal_stop(seq, stops, rev=False):
    if rev:
        seq = rev_compl(seq)
    for i in range(0, len(seq), 3):
        if seq[i:i+3] in stops:
            return i+1
    return None

def isint(val):
    try: 
        int(val)
        return True
    except ValueError:
        return False

def get_freq_from_tsv(fname):
    freqs = defaultdict(int)
    with open(fname, "r") as handle:
        handle.readline()
        cols = [x.strip() for x in handle.readline().strip().split("\t")]
        l = len(cols)
        for line in handle:
            for i, val in enumerate(line.strip().split("\t")):
                if i == l:
                    break
                if isint(val):
                    freqs[cols[i] + "_" + val] += 1
    return freqs           

def main():
    args = parse_args()
    if args.matrix:
        freqs = get_freq_from_tsv(args.matrix)
    
    args.start = tuple(args.start)
    args.stop = tuple(args.stop)

    print("gene\tallele\tfrequency\tclass\tdirection\tgene_length\tstop_absolute_position\tstop_relative_position")
    for fname in args.fasta:
        excluded = set()
        gene = os.path.splitext(os.path.basename(fname))[0]
        complete = 0
        truncated = 0
        for id, seq in iter_fasta(fname):
            if not args.matrix:
                freq ="-"
            else:
                freq = freqs[gene+"_"+id]
            f = 0
            dir = None
            if has_start_and_stop(seq, args.start, args.stop):
                f += 1
                rev = False
                dir = "+"
            elif has_start_and_stop(seq, args.start, args.stop, True):
                f += 1
                rev = True
                dir = "-"
            if f == 0:
                print(gene + "\t" + id + "\t" + str(freq) + "\tignored\t-\t-\t-\t-")
            else:
                l = len(seq)
                s = first_internal_stop(seq, args.stop, rev)
                rel_pos = round((s+2)/l*100, 2)
                if l - s == 2:
                    cl = "complete"
                else:
                    cl = "truncated"
                print(gene + "\t" + id + "\t" + str(freq) + "\t" + cl + "\t" +  dir + "\t" + str(l) + "\t" + str(s) + "\t" + str(rel_pos))
    

if __name__ == "__main__":
	main()
