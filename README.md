# LMO essential genes

A script to search for allele sequences containing premature stop codons, as used in our study  "Listeria monocytogenes genes supporting growth under standard laboratory cultivation conditions and during macrophage infection"

## Dependencies

- Python 3.9
- Biopython 

## Usage

```
usage:  path/to/count_truncated_allels.py [-h] [--start STR [STR ...]] [--stop STR [STR ...]] [--matrix FILE]
        [--version]
        FASTA_FILE [FASTA_FILE ...]

positional arguments:
  FASTA_FILE            fasta files containing allele sequences (one per gene)

optional arguments:
  -h, --help            show this help message and exit
  --start STR [STR ...]
                        start codon(s) (default: 'TTG', 'GTG', 'ATA', 'ATG',
                        'ATC', 'ATT', 'CTG')
  --stop STR [STR ...]  stop codon(s) (default: 'TAA', 'TAG', 'TGA')
  --matrix FILE         calculate allele frequencies based on tab-delimited
                        allele matrix e.g. provided by Seqsphere+ (default:
                        none)
  --version             show program's version number and exit
```

